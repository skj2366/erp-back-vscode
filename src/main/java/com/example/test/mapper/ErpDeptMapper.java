package com.example.test.mapper;

import com.example.test.vo.ErpDeptVO;

import org.mybatis.spring.annotation.MapperScan;

/**
 * ErpDeptMapper
 */
@MapperScan
public interface ErpDeptMapper {
    ErpDeptVO selectErpDeptByCode(String edCode);
}