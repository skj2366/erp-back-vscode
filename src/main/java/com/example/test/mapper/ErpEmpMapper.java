package com.example.test.mapper;

import java.util.List;

import com.example.test.vo.ErpEmpVO;
import com.github.pagehelper.Page;

import org.mybatis.spring.annotation.MapperScan;

/**
 * ErpEmpMapper
 */
@MapperScan
public interface ErpEmpMapper {

    List<ErpEmpVO> selectErpEmpList();
    int insertErpEmp(ErpEmpVO ee);
    ErpEmpVO selectErpEmpByNo(String eeEmpno);
    Integer deleteErpEmp(String eeEmpno);
    Integer updateErpEmp(ErpEmpVO ee);
    ErpEmpVO selectErpEmpByEmpno(ErpEmpVO ee);
    Page<ErpEmpVO> selectPageList();
    Integer insertEmp(ErpEmpVO ee);
    Integer updateEmp(ErpEmpVO ee);
    
}