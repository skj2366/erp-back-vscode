package com.example.test.mapper;

import java.util.List;

import com.example.test.vo.TourBoardVO;

import org.mybatis.spring.annotation.MapperScan;

@MapperScan
public interface TourBoardMapper {
	List<TourBoardVO> selectTourBoardList();
	int insertTourBoard(TourBoardVO tb);
	TourBoardVO selectTourBoardByNum(int tbNum);
	Integer deleteTourBoard(int tbNum);
	int updateTourBoard(TourBoardVO tb);
	int updateScore(int tbNum);
}