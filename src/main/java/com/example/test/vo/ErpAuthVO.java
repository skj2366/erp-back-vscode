package com.example.test.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

/**
 * ErpAuth
 */
@Data
@Alias("ea")
public class ErpAuthVO {

    private String eeEmpno;
    
}