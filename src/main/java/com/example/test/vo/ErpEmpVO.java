package com.example.test.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

/**
 * ErpEmpVO
 */
@Data
@Alias("ee")
public class ErpEmpVO {

    private Integer eeNum;
    private String eeName;
    // private Integer eeAge;
    private String eePhone1;
    private String eePhone2;
    private String eeSsn;
    private String eeEmpno;
    private String eeCredat;
    private String eeQuitdat;
    private String eeEmail;
    private String eePwd;
    private String eeAddr1;
    private String eeAddr2;
    private String eeImg;
    private Integer eeSal;
    private String edCode;
    private String eeBankCode;
    private String eeBankName;
    private String elCode;
    private String token;
    private MultipartFile eeImgFile;
    private String eePwdCheck;
    private String eeZipcode;
    private String eeFirstPwd;

    
}