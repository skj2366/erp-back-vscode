package com.example.test.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

/**
 * ErpLevel
 */
@Data
@Alias("el")
public class ErpLevelVO {

    private String elCode;
    private String elName;
    
}