package com.example.test.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Alias("tb")
public class TourBoardVO {
	private Integer tbNum;
	private String tbLoc;
	private Integer tbScore;
	private String tbTitle;
	private String tbContent;
	private String tbWriter;
	private String tbCredat;
	private String tbCretim;
	private Integer tbLike;
	private Integer tbCnt;
	private MultipartFile tbPath;
}