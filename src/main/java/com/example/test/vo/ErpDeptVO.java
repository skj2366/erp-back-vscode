package com.example.test.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

/**
 * ErpDept
 */
@Data
@Alias("ed")
public class ErpDeptVO {

    private String edName;
    private String edCode;
    
}