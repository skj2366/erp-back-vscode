package com.example.test.filter;

import java.io.IOException;
// import java.util.Enumeration;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import com.example.test.auth.MakeJWT;
import com.example.test.vo.ErpEmpVO;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import lombok.extern.slf4j.Slf4j;

// @Component
@Slf4j
public class CheckJWTFilter extends GenericFilterBean {

	@Resource
	private MakeJWT mjwt;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest)request;
		String method = req.getMethod();
		if(!method.equals("OPTIONS") && !req.getRequestURI().equals("/login") && !req.getRequestURI().equals("/signup") && !req.getRequestURI().equals("/img/**") && !req.getRequestURI().equals("/resources/img/**")){
			// Enumeration<String> names = req.getHeaderNames();
			String eeEmpno = req.getHeader("X-AUTH-ID");
			String token = req.getHeader("X-AUTH-TOKEN");
			// String eeImg = req.getHeader("X-AUTH-IMG");
			try {
				ErpEmpVO ee = new ErpEmpVO();
				ee.setEeEmpno(eeEmpno);
				// ee.setEeImg(eeImg);
				mjwt.checkJWT(token, ee);
			}catch(Exception e) {
				log.error("JWT error => {}",e);
				throw new ServletException("올바르지 않은 토큰값 입니다.");
			}
		}
		chain.doFilter(request, response);
	}

}
