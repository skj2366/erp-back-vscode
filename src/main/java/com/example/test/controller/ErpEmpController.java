package com.example.test.controller;

import java.util.List;

import javax.annotation.Resource;

import com.example.test.service.ErpEmpService;
import com.example.test.vo.ErpEmpVO;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class ErpEmpController {

    @Resource
    private ErpEmpService ees;

    @CrossOrigin("*")
    @GetMapping("/emps")
    public @ResponseBody List<ErpEmpVO> getEmpErps(){
        log.debug("getErpEmpList => {}");
        return ees.selectErpEmpList();
    }

    @CrossOrigin("*")
    @GetMapping("/emp/{eeEmpno}")
    public @ResponseBody ErpEmpVO getEmpErp(@PathVariable("eeEmpno") String eeEmpno){
        return ees.selectErpEmpByNo(eeEmpno);
    }

    @CrossOrigin("*")
    @PostMapping("/signup2")
    public @ResponseBody int insertErpEmp(@RequestBody ErpEmpVO ee){
        log.debug("insertErpEmp => {}",ee);
        return ees.signUp(ee);
    }

    @CrossOrigin(origins="*")
	@PostMapping("/login")
	public @ResponseBody ErpEmpVO doLogin(@RequestBody ErpEmpVO ee) {
		log.info("param => {}",ee);
		return ees.login(ee);
    }
    

    @CrossOrigin("*")
    @PostMapping("/signup")
    public @ResponseBody Integer doSign(ErpEmpVO ee){
        
        // String credat = ee.getEeCredat();
        // if(credat.indexOf("-")!= -1){
            
        // }
        // String credat =  ee.getEeCredat();
        // if(만약 대시가 있으면 ){
        //     대시 빼 
        //     ee.setEeCredat(credat);
        // }else{
        //     그냥 패스 
        // }
        
        log.info(" Controller ErpEmpVO=>{}",ee);
		String path = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/img/")
				.toUriString();
		log.info("path=>{}",path);
        // return ees.insertErpEmpFile(ee);
        return ees.insertEmp(ee);
    }

    @CrossOrigin("*")
    @GetMapping("/page")
    public @ResponseBody PageInfo<ErpEmpVO> selectPageList(){
        Page<ErpEmpVO> empList = ees.selectPageList();
        log.info("empList => {}",empList);

        return new PageInfo<>(empList);
    }

    @CrossOrigin("*")
    @DeleteMapping("/emp/{eeEmpno}")
    public @ResponseBody Integer deleteEmp(@PathVariable("eeEmpno") String eeEmpno){
        log.debug("eeEmpno => {}",eeEmpno);
        return ees.deleteErpEmp(eeEmpno);
    }

    @CrossOrigin("*")
    @PutMapping("/emp/{eeEmpno}")
    public @ResponseBody Integer updateEmp(@PathVariable("eeEmpno") String eeEmpno, @RequestBody ErpEmpVO ee){
        log.info("ErpEmpVO put => {}",ee);
        return ees.updateErpEmp(ee);
    }

    @CrossOrigin("*")
    @PostMapping("/emp/{eeEmpno}")
    public @ResponseBody Integer updateEmpFile(@PathVariable("eeEmpno") String eeEmpno, ErpEmpVO ee){
        log.info("ErpEmpVO put => {}",ee);
        return ees.updateEmp(ee);
    }

    @CrossOrigin("*")
    @PutMapping("/emp")
	public @ResponseBody Integer updateEmp2(@RequestBody ErpEmpVO ee) {
		return ees.updateEmp(ee);
	}
    
}