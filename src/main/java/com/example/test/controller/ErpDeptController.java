package com.example.test.controller;

import javax.annotation.Resource;

import com.example.test.service.ErpDeptService;
import com.example.test.vo.ErpDeptVO;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import lombok.extern.slf4j.Slf4j;

/**
 * ErpDeptController
 */
@Controller
@Slf4j
public class ErpDeptController {

    @Resource
    private ErpDeptService eds;

    // @CrossOrigin
    // @GetMapping("/dept")
    // public ErpDep

    @CrossOrigin("*")
    @GetMapping("/dept/{edCode}")
    public ErpDeptVO getEdName(@PathVariable("edCode") String edCode){
        log.debug("edCode => {}",edCode);
        return eds.selectErpDeptByCode(edCode);
    }
    
}