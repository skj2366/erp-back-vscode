package com.example.test.controller;

import java.util.List;

import javax.annotation.Resource;

import com.example.test.service.TourBoardService;
import com.example.test.vo.TourBoardVO;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class TourBoardController {

	@Resource
	private TourBoardService tbs;
	
	@CrossOrigin(origins="*")
	@GetMapping("/tours")
	public @ResponseBody List<TourBoardVO> getTourList() {
		log.debug("getTourList => {}");
		return tbs.selectTourBoardList();
	}
	
	@CrossOrigin(origins="*")
	@PostMapping("/inserttour")
	public @ResponseBody int insertTourBoard(@RequestBody TourBoardVO tb) {
		log.debug("insertTourBoard => {}",tb);
		return tbs.insertTourBoard(tb);
	}
	
	@CrossOrigin(origins="*")
	@GetMapping("/tour")
	public @ResponseBody TourBoardVO getTourByNum(@RequestParam int tbNum) {
		return tbs.selectTourBoardByNum(tbNum);
	}
	
	@CrossOrigin(origins="*")
	@DeleteMapping("/tour")
	public @ResponseBody Integer deleteTourByNum(@RequestParam int tbNum) {
		log.debug("delete => {}", tbNum);
		return tbs.deleteTourBoard(tbNum);
	}
	
	@CrossOrigin("*")
	@PutMapping("/tour")
	public @ResponseBody int updateTourBoard(@RequestBody TourBoardVO tb) {
		log.debug("{}",tb);
		return tbs.updateTourBoard(tb);
	}
	
	@CrossOrigin("*")
	@PutMapping("/cnt")
	public @ResponseBody int updateLike(@RequestParam int tbNum) {
		log.debug("score => {}",tbNum);
		return tbs.updateScore(tbNum);
	}
	
}
