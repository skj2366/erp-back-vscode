package com.example.test.config;

import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport{
    
    @Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry
		.addResourceHandler("/img/**")
		.addResourceLocations("/resources/img/")
		.setCacheControl(CacheControl.maxAge(2, TimeUnit.HOURS).cachePublic());
		
		registry
		.addResourceHandler("/video/**")//해당 서버로 들어오는 URL 패턴이라고 보면된다.
		.addResourceLocations("/resources/video/")//이 서버가 바라보고 있는 리소시즈에 비디오라는 폴더를 로케이션으로 바라보게 된다.
		.setCacheControl(CacheControl.maxAge(2, TimeUnit.HOURS).cachePublic());
		
	} 

}