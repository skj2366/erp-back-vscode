package com.example.test.service;

import java.util.List;

import com.example.test.vo.TourBoardVO;

public interface TourBoardService {

	List<TourBoardVO> selectTourBoardList();
	int insertTourBoard(TourBoardVO tb);
	TourBoardVO selectTourBoardByNum(int tbNum);
	Integer deleteTourBoard(int tbNum);
	int updateTourBoard(TourBoardVO tb);
	int updateScore(int tbNum);
}
