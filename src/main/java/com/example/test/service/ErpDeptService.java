package com.example.test.service;

import com.example.test.vo.ErpDeptVO;

/**
 * ErpDeptService
 */
public interface ErpDeptService {

    ErpDeptVO selectErpDeptByCode(String edCode);
    
}