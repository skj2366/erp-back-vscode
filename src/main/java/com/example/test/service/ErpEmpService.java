package com.example.test.service;

import java.util.List;

import com.example.test.vo.ErpEmpVO;
import com.github.pagehelper.Page;

public interface ErpEmpService {

    List<ErpEmpVO> selectErpEmpList();
    int insertErpEmp(ErpEmpVO ee);
    ErpEmpVO selectErpEmpByNo(String eeEmpno);
    Integer deleteErpEmp(String eeEmpno);
    Integer updateErpEmp(ErpEmpVO ee);
    ErpEmpVO login(ErpEmpVO ee);
    int signUp(ErpEmpVO ee);
    Integer insertErpEmpFile(ErpEmpVO ee);
    Page<ErpEmpVO> selectPageList();
    Integer insertEmp(ErpEmpVO ee);
    Integer updateEmp(ErpEmpVO ee);
    
}