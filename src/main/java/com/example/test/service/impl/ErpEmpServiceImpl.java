package com.example.test.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import javax.annotation.Resource;

import com.example.test.auth.MakeJWT;
import com.example.test.auth.SHAEncoder;
import com.example.test.mapper.ErpEmpMapper;
import com.example.test.service.ErpEmpService;
import com.example.test.vo.ErpEmpVO;
import com.github.pagehelper.Page;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ErpEmpServiceImpl implements ErpEmpService {

    // private final String BASE = "D:\\study\\nodeworks\\erp-back-vscode\\src\\main\\resources\\webapp\\resources\\upload\\";
    private final String BASE = "D:\\study\\nodeworks\\erp-back-vscode\\src\\main\\webapp\\resources\\img\\";

    @Resource
    private ErpEmpMapper eem;
    @Resource
    private MakeJWT mjwt;

    @Override
    public List<ErpEmpVO> selectErpEmpList() {
        return eem.selectErpEmpList();
    }

    @Override
    public int insertErpEmp(ErpEmpVO ee) {
        return eem.insertErpEmp(ee);
    }

    @Override
    public ErpEmpVO selectErpEmpByNo(String eeEmpno) {
        return eem.selectErpEmpByNo(eeEmpno);
    }

    @Override
    public Integer deleteErpEmp(String eeEmpno) {
        ErpEmpVO ee = eem.selectErpEmpByNo(eeEmpno);
        if(eem.deleteErpEmp(eeEmpno) == 1){
            if(ee.getEeImg() != null){
                File orgFile = new File(BASE + ee.getEeImg());
                if(orgFile.exists()){
                    orgFile.delete();
                }
            }
            return 1;
        }
        return null;
    }

    @Override
    public Integer updateErpEmp(ErpEmpVO ee) {
        String pwd = SHAEncoder.encode(ee.getEePwd());
        ee.setEePwd(pwd);
        return eem.updateErpEmp(ee);
    }

    @Override
    public ErpEmpVO login(ErpEmpVO ee) {
        System.out.println("ee.getEePwd : " + ee.getEePwd());
        ee.setEePwd(SHAEncoder.encode(ee.getEePwd()));
        ee = eem.selectErpEmpByEmpno(ee);
        if (ee != null) {
            ee.setToken(mjwt.makeJWT(ee));
        }
        return ee;
    }

    @Override
    public int signUp(ErpEmpVO ee) {
        String pwd = SHAEncoder.encode(ee.getEePwd());
        ee.setEePwd(pwd);
        return eem.insertErpEmp(ee);
    }

    @Override
    public Integer insertErpEmpFile(ErpEmpVO ee) {
        MultipartFile mf = ee.getEeImgFile();
        log.info("mf =>{}", mf);
        String fileName = mf.getOriginalFilename();
        log.info("fileName => {}", fileName);
        String extName = FilenameUtils.getExtension(fileName);
        log.info("extName => {}", extName);
        // String extName = "";
        String reName = Long.toString(System.nanoTime());
        reName += "." + extName;

        File targetFile = new File(BASE + reName);

        try {
            Files.copy(mf.getInputStream(), targetFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ee.setEeImg(reName);
        String pwd = SHAEncoder.encode(ee.getEePwd());
        ee.setEePwd(pwd);
        return eem.insertErpEmp(ee);
    }

    @Override
    public Page<ErpEmpVO> selectPageList() {
        return eem.selectPageList();
    }

    @Override
    public Integer insertEmp(ErpEmpVO ee) {

        if(ee.getEeCredat().indexOf("-")!=-1) {
			ee.setEeCredat(ee.getEeCredat().replace("-", ""));
		}
		
        MultipartFile mf = ee.getEeImgFile();
        log.info("mf =>{}", mf);
        String fileName = mf.getOriginalFilename();
        log.info("fileName => {}", fileName);
        String extName = FilenameUtils.getExtension(fileName);
        log.info("extName => {}", extName);
        // String extName = "";
        String reName = Long.toString(System.nanoTime());
        reName += "." + extName;

        File targetFile = new File(BASE + reName);

        try {
            Files.copy(mf.getInputStream(), targetFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ee.setEeImg(reName);

        if(ee.getEeSsn()!=null) {
            String pwd = ee.getEeSsn();
			log.info("pwd=>{}",pwd);
			if(pwd.indexOf("-")!=-1) {
                pwd = pwd.replace("-", "");
                ee.setEeFirstPwd(pwd);
            }
            pwd = SHAEncoder.encode(pwd);
            log.info("pwd encode =>{}",pwd);
            ee.setEePwd(pwd);
		}
        return eem.insertEmp(ee);
    }

    @Override
    public Integer updateEmp(ErpEmpVO ee) {
        log.debug("this emp => {}",ee);
        System.out.println("asdasdsad " + ee.getEeImgFile());
        // MultipartFile mf = ee.getEeImgFile();
		// if(!ee.getEeImgFile().equals(null)) {
        //     log.debug("getEeImgFile => {}", ee.getEeImgFile());
        //     log.info("mf =>{}", mf);
        //     String fileName = mf.getOriginalFilename();
        //     log.info("fileName => {}", fileName);
        //     String extName = FilenameUtils.getExtension(fileName);
        //     log.info("extName => {}", extName);
        //     // String extName = "";
        //     String reName = Long.toString(System.nanoTime());
        //     reName += "." + extName;

        //     File targetFile = new File(BASE + reName);

        //     try {
        //         Files.copy(mf.getInputStream(), targetFile.toPath());
        //     } catch (IOException e) {
        //         e.printStackTrace();
        //     }

        //     ee.setEeImg(reName);
        // }


        String pwd = ee.getEePwd();
		ee.setEePwd(SHAEncoder.encode(pwd));

        MultipartFile mf = ee.getEeImgFile();
        log.info("mf =>{}", mf);
        String fileName = mf.getOriginalFilename();
        log.info("fileName => {}", fileName);
        String extName = FilenameUtils.getExtension(fileName);
        log.info("extName => {}", extName);
        // String extName = "";
        String reName = Long.toString(System.nanoTime());
        reName += "." + extName;

        File targetFile = new File(BASE + reName);

        try {
            Files.copy(mf.getInputStream(), targetFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ee.setEeImg(reName);
		ErpEmpVO erpMember = eem.selectErpEmpByNo(ee.getEeEmpno());
		
		if (eem.updateEmp(ee) == 1) {
			if (erpMember.getEeImg() != null) {
				File erpFile = new File(BASE + erpMember.getEeImg());
				if (erpFile.exists()) {
					erpFile.delete();
				} 
			}
			return 1;

		} else {
			targetFile.delete();
		}

		return null;

        // ee.setEeImg(reName);

        // String pwd = ee.getEePwd();
		// ee.setEePwd(SHAEncoder.encode(pwd));
        
		// return eem.updateEmp(ee);
    }


}