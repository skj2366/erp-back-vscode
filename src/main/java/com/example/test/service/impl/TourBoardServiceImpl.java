package com.example.test.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.example.test.mapper.TourBoardMapper;
import com.example.test.service.TourBoardService;
import com.example.test.vo.TourBoardVO;

import org.springframework.stereotype.Service;


@Service

public class TourBoardServiceImpl implements TourBoardService {

	@Resource
	private TourBoardMapper tbm;
	// private final String BASE_PATH = "D:\\study\\springb\\sp\\src\\main\\webapp\\resource\\files\\imgs\\";
	
	@Override
	public List<TourBoardVO> selectTourBoardList() {
		return tbm.selectTourBoardList();
	}

	@Override
	public int insertTourBoard(TourBoardVO tb) {
//		MultipartFile mf = tb.getTbPath();
//		String originName = mf.getOriginalFilename();
//		String extName = "";
//		if(originName.lastIndexOf(".")!=-1) {
//			extName = originName.substring(originName.lastIndexOf("."));
//		}
//		String fileName = System.currentTimeMillis() + extName;
//		log.info("fileName=>{}",fileName);
//		File saveFile = new File(BASE_PATH + fileName);
//		try {
//			Files.copy(mf.getInputStream(), saveFile.toPath());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		return tbm.insertTourBoard(tb);
	}

	@Override
	public TourBoardVO selectTourBoardByNum(int tbNum) {
		return tbm.selectTourBoardByNum(tbNum);
	}

	@Override
	public Integer deleteTourBoard(int tbNum) {
		return tbm.deleteTourBoard(tbNum);
	}

	@Override
	public int updateTourBoard(TourBoardVO tb) {
		return tbm.updateTourBoard(tb);
	}

	@Override
	public int updateScore(int tbNum) {
		return tbm.updateScore(tbNum);
	}

}
