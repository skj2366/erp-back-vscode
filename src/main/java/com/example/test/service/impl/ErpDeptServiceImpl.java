package com.example.test.service.impl;

import javax.annotation.Resource;

import com.example.test.mapper.ErpDeptMapper;
import com.example.test.service.ErpDeptService;
import com.example.test.vo.ErpDeptVO;

import org.springframework.stereotype.Service;

/**
 * ErpDeptServiceImpl
 */
@Service
public class ErpDeptServiceImpl implements ErpDeptService {

    @Resource
    private ErpDeptMapper edm;

    @Override
    public ErpDeptVO selectErpDeptByCode(String edCode) {
        return edm.selectErpDeptByCode(edCode);
    }

    
}