package com.example.test.auth;

import java.util.Calendar;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.test.vo.ErpEmpVO;

import org.springframework.stereotype.Component;


import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MakeJWT {

	private static final Date ISSUE_DATE;
	private static final Date EXPIRE_DATE;
	private static final String SALT = "osf";
	static { 
		Calendar calendar = Calendar.getInstance();
		ISSUE_DATE = calendar.getTime();
		calendar.add(Calendar.DATE, 10);
		EXPIRE_DATE = calendar.getTime();
	}
	
	public String makeJWT(ErpEmpVO ee) {
		String jwt = JWT.create().withIssuer(ee.getEeEmpno())
				.withIssuedAt(ISSUE_DATE).withExpiresAt(EXPIRE_DATE).sign(Algorithm.HMAC256(SALT));
		log.info("jwt=>{}",jwt);
		return jwt;
	}
	
	public void checkJWT(String token, ErpEmpVO ee) {
		JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SALT)).withIssuer(ee.getEeEmpno()).build();
		DecodedJWT decode = verifier.verify(token);
		log.info("decode => {}",decode);
		log.info("issuer => {}",decode.getIssuer());
		log.info("issue date => {}",decode.getExpiresAt());
	}
	
	// public static void main(String[] args) {
	// 	MakeJWT mjwt = new MakeJWT();
	// 	UserInfoVO ui = new UserInfoVO();
	// 	ui.setUiId("test");
	// 	String jwt = mjwt.makeJWT(ui);
	// 	mjwt.checkJWT(jwt, ui);
	// }
}
